## Sample yaml code for dup template / job

.gitlab-ci.yml demostrates how to include a same template file My-Template.gitlab-ci.yml twice with the same job dast

proj_lang_specific.gitlab-ci.yml and common.gitlab-ci.yml files both use template My-Template.gitlab-ci.yml with dast job


GitLab is working on this issue: Can't extend CI YAML templates in a reusable and non-breaking way (#28987), plan to deliver in release 14. https://gitlab.com/gitlab-org/gitlab/-/issues/28987 

It is related to Allow triangular gitlab-ci imports https://gitlab.com/gitlab-org/gitlab-foss/-/issues/63982
